CREATE TABLE EMPLOYEE (
     employee_number text ,
     supervisor text ,
     employee_first_name text ,
     employee_address text ,
     employee_phone int ,
     employee_address_2 text ,
     soc_sec_number int ,
     hire_date date ,
     salary int ,
     email text ,
     store_number int ,
     PRIMARY KEY (employee_number)
) 
WITH comment = 'EMPLOYEE is a person that works for the VIDEO STORE';

CREATE TABLE MOVIE (
     movie_number int ,
     movie_title text ,
     movie_director text ,
     description text ,
     star_1_name text ,
     rating text ,
     star_2_name text ,
     genre text ,
     rental_rate decimal ,
     movie_url text ,
     movie_clip blob ,
     PRIMARY KEY (movie_number)
) 
WITH comment = 'A MOVIE is any video that can be rented” for the entity “MOVIE';

CREATE TABLE STORE (
     store_number int ,
     store_manager text ,
     store_address text ,
     store_phone int ,
     store_city text ,
     store_state text ,
     store_zip_code int ,
     store_address_2 text ,
     PRIMARY KEY (store_number)
) 
WITH comment = 'A STORE is one of the MOVIE STORES where a customer can rent a movie';

CREATE TABLE MOVIE_RENTAL_RECORD (
     rental_record_date date ,
     movie_copy_number int ,
     movie_number int ,
     soc_sec_number int ,
     employee_phone int ,
     customer_number int ,
     rental_date date ,
     due_date date ,
     rental_status text ,
     overdue_charge int ,
     rental_rate int ,
     payment_transaction_number int ,
     PRIMARY KEY ((rental_record_date, soc_sec_number, employee_phone, movie_copy_number, movie_number, customer_number, customer_number))
) 
WITH comment = 'A MOVIE RENTAL RECORD is a record that  is kept on every MOVIE in the STORE  on who and when a MOVIE is rented';

CREATE TABLE MOVIE_COPY (
     movie_copy_number int ,
     movie_number int ,
     general_condition text ,
     movie_format text ,
     PRIMARY KEY ((movie_copy_number, movie_number))
) 
WITH comment = 'A MOVIE COPY is a single copy of a MOVIE';

CREATE TABLE CUSTOMER (
     customer_number int ,
     customer_address text ,
     customer_city text ,
     customer_first_name text ,
     customer_last_name text ,
     customer_state text ,
     customer_zip_code int ,
     email text ,
     PRIMARY KEY (customer_number)
) 
WITH comment = 'A CUSTOMER is a person or organization who has rented a movie within the past year.';

CREATE TABLE CUSTOMER_CREDIT (
     customer_number int ,
     credit_card int ,
     credit_card_exp int ,
     status_code text ,
     PRIMARY KEY (customer_number)
) 
WITH comment = 'CUSTOMER CREDIT is the status of available credit for a CUSTOMER';

CREATE TABLE MOVIE_STORE (
     movie_number int ,
     store_number int ,
     PRIMARY KEY ((movie_number, store_number))
) ;

CREATE TABLE PAYMENT (
     payment_transaction_number int ,
     payment_type text ,
     customer_number int ,
     customer_no int ,
     payment_amount decimal ,
     payment_date date ,
     payment_status text ,
     employee_number text ,
     check_bank_number int ,
     check_number int ,
     epay_vendor_number int ,
     epay_account_number int ,
     credit_card_number text ,
     credit_card_exp timestamp ,
     credit_card_type text ,
     PRIMARY KEY (payment_transaction_number)
) 
WITH comment = 'A PAYMENT is received when a customer rents a movie';