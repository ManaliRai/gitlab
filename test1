
CREATE TABLE [tbl1]
( 
	[a]                  char(18)  NOT NULL ,
	[b]                  char(18)  NOT NULL ,
	[c]                  char(18)  NULL ,
	[d]                  char(18)  NULL ,
	[e]                  char(18)  NULL 
)
go

CREATE TABLE [tbl2]
( 
	[aa]                 char(18)  NOT NULL ,
	[bb]                 char(18)  NULL ,
	[a]                  char(18)  NOT NULL ,
	[b]                  char(18)  NOT NULL 
)
go

ALTER TABLE [tbl1]
	ADD CONSTRAINT [XPKtbl1] PRIMARY KEY  CLUSTERED ([a] ASC,[b] ASC)
go

ALTER TABLE [tbl2]
	ADD CONSTRAINT [XPKtbl2] PRIMARY KEY  CLUSTERED ([aa] ASC,[a] ASC,[b] ASC)
go


ALTER TABLE [tbl2]
	ADD CONSTRAINT [R_1] FOREIGN KEY ([a],[b]) REFERENCES [tbl1]([a],[b])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

CREATE TRIGGER tD_tbl1 ON tbl1 FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on tbl1 */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* tbl1  tbl3 on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="0001d515", PARENT_OWNER="", PARENT_TABLE="tbl1"
    CHILD_OWNER="", CHILD_TABLE="tbl3"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_2", FK_COLUMNS="a""b" */
    IF EXISTS (
      SELECT * FROM deleted,tbl3
      WHERE
        /*  %JoinFKPK(tbl3,deleted," = "," AND") */
        tbl3.a = deleted.a AND
        tbl3.b = deleted.b
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete tbl1 because tbl3 exists.'
      GOTO error
    END

    /* erwin Builtin Trigger */
    /* tbl1  tbl2 on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="tbl1"
    CHILD_OWNER="", CHILD_TABLE="tbl2"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="a""b" */
    IF EXISTS (
      SELECT * FROM deleted,tbl2
      WHERE
        /*  %JoinFKPK(tbl2,deleted," = "," AND") */
        tbl2.a = deleted.a AND
        tbl2.b = deleted.b
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete tbl1 because tbl2 exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tU_tbl1 ON tbl1 FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on tbl1 */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insa char(18), 
           @insb char(18),
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* tbl1  tbl3 on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="0001f6bf", PARENT_OWNER="", PARENT_TABLE="tbl1"
    CHILD_OWNER="", CHILD_TABLE="tbl3"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_2", FK_COLUMNS="a""b" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(a) OR
    UPDATE(b)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,tbl3
      WHERE
        /*  %JoinFKPK(tbl3,deleted," = "," AND") */
        tbl3.a = deleted.a AND
        tbl3.b = deleted.b
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update tbl1 because tbl3 exists.'
      GOTO error
    END
  END

  /* erwin Builtin Trigger */
  /* tbl1  tbl2 on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="tbl1"
    CHILD_OWNER="", CHILD_TABLE="tbl2"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="a""b" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(a) OR
    UPDATE(b)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,tbl2
      WHERE
        /*  %JoinFKPK(tbl2,deleted," = "," AND") */
        tbl2.a = deleted.a AND
        tbl2.b = deleted.b
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update tbl1 because tbl2 exists.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tD_tbl2 ON tbl2 FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on tbl2 */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* tbl1  tbl2 on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="000131b7", PARENT_OWNER="", PARENT_TABLE="tbl1"
    CHILD_OWNER="", CHILD_TABLE="tbl2"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="a""b" */
    IF EXISTS (SELECT * FROM deleted,tbl1
      WHERE
        /* %JoinFKPK(deleted,tbl1," = "," AND") */
        deleted.a = tbl1.a AND
        deleted.b = tbl1.b AND
        NOT EXISTS (
          SELECT * FROM tbl2
          WHERE
            /* %JoinFKPK(tbl2,tbl1," = "," AND") */
            tbl2.a = tbl1.a AND
            tbl2.b = tbl1.b
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last tbl2 because tbl1 exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tU_tbl2 ON tbl2 FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on tbl2 */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insaa char(18), 
           @insa char(18), 
           @insb char(18),
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* tbl1  tbl2 on child update no action */
  /* ERWIN_RELATION:CHECKSUM="0001619f", PARENT_OWNER="", PARENT_TABLE="tbl1"
    CHILD_OWNER="", CHILD_TABLE="tbl2"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="a""b" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(a) OR
    UPDATE(b)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,tbl1
        WHERE
          /* %JoinFKPK(inserted,tbl1) */
          inserted.a = tbl1.a and
          inserted.b = tbl1.b
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    
    IF @validcnt + @nullcnt != @numrows
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update tbl2 because tbl1 does not exist.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go



