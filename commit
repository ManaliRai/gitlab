CREATE TABLE BookAuthor (
     Author_Identification text ,
     Book_Identification text ,
     PRIMARY KEY ((Author_Identification, Book_Identification))
) 
WITH comment = 'This is the Author or Authors name for any book available for purchase.';

CREATE TABLE Store_Name (
     Store_Identification text ,
     Store_Name text ,
     Store_Address text ,
     Store_City text ,
     Store_State ascii ,
     Store_Zip_Code ascii ,
     Region_Identification text ,
     PRIMARY KEY (Store_Identification)
) 
WITH comment = 'The name of the company store name that has books available for sale.';

CREATE TABLE Purchase_Order (
     Order_Number int ,
     Store_Identification text ,
     Order_Date date ,
     Payment_Terms text ,
     Customer_Identification text ,
     PRIMARY KEY (Order_Number)
) 
WITH comment = 'The general document that identifies information about a customer that has book item(s) placed in an order.';

CREATE TABLE Royalty (
     Royalty_Identification text ,
     Low_Range int ,
     High_Range int ,
     Royalty_Amount varint ,
     PRIMARY KEY (Royalty_Identification)
) 
WITH comment = 'Information about the royalty agreement the company has established with the author of a book.';

CREATE TABLE Discount (
     Discount_Type text ,
     Low_Quantity smallint ,
     High_Quantity smallint ,
     Discount_Percent  ,
     PRIMARY KEY (Discount_Type)
) 
WITH comment = 'Discount information that is offered to customers either through incentives or promotions offered by the company when a purchase order is established.';

CREATE TABLE Job (
     Job_Identification text ,
     Job_Description text ,
     Minimum_Level tinyint ,
     Maximum_Level tinyint ,
     PRIMARY KEY (Job_Identification)
) 
WITH comment = 'The job position title for an employee in the company. ';

CREATE TABLE Publisher_Logo (
     Publisher_Identification text ,
     Publisher_Logo text ,
     Publisher_Public_Relations_Information text ,
     PRIMARY KEY (Publisher_Identification)
) 
WITH comment = 'The signature logo for the company that hold the publishing rights for a book. ';

CREATE TABLE Employee (
     Employee_Identification text ,
     Employee_First_Name text ,
     Employee_Middle_Initial text ,
     Employee_Last_Name text ,
     Job_Identification text ,
     Current_Employee_Job_Title tinyint ,
     Employee_Hire_Date date ,
     PRIMARY KEY (Employee_Identification)
) 
WITH comment = 'The employee name and pertinent information about the employee that is used to support this publication application. This information is copied from the company's HR system.';

CREATE TABLE Author (
     Author_Identification text ,
     Author_Last_Name text ,
     Author_First_Name text ,
     Author_Phone_Number int ,
     Author_Address text ,
     Author_City text ,
     Author_State ascii ,
     Author_Zip_Code ascii ,
     Contract smallint ,
     PRIMARY KEY (Author_Identification)
) 
WITH comment = 'Contact information about the author of a book. If more than one author is involved, then the primary contact author is recorded here.';

CREATE TABLE Publisher (
     Publisher_Identification text ,
     Publisher_Name text ,
     Publisher_Address text ,
     Publisher_City text ,
     Publisher_State ascii ,
     Publisher_Zip_Code ascii ,
     PRIMARY KEY (Publisher_Identification)
) 
WITH comment = 'The contact information for the publishing company that has the publishing rights to a book. ';

CREATE TABLE Book (
     Book_Identification text ,
     Book_Name text ,
     Book_Type text ,
     Publisher_Identification text ,
     MSRP_Price int ,
     Advance int ,
     Royalty_Terms int ,
     Book_Note text ,
     Publication_Date timestamp ,
     PRIMARY KEY (Book_Identification)
) 
WITH comment = 'The title for a book our company has the rights to sell.';

CREATE TABLE Order_Item (
     Order_Number int ,
     Item_Sequence_Number int ,
     Book_Identification text ,
     Order_Quantity smallint ,
     Discount_Type text ,
     Order_Discount_Amount decimal ,
     Order_Price decimal ,
     PRIMARY KEY ((Item_Sequence_Number, Order_Number))
) 
WITH comment = 'Specific line item entries for a purchase order form a customer.';

CREATE TABLE Royalty_Payment (
     Author_Identification text ,
     Book_Identification text ,
     Royalty_Identification text ,
     Payment_Date date ,
     Payment_Amount decimal ,
     PRIMARY KEY ((Royalty_Identification, Author_Identification, Book_Identification))
) 
WITH comment = 'The royalty agreement our company has established with the book's author in terms of the royalty agreement contract we negotiated with the author.';

CREATE TABLE Book_YTD_Sales (
     Book_Identification text ,
     Year_To_Date_Sales_Amount decimal ,
     Year_To_Date_Sales_Date date ,
     PRIMARY KEY (Book_Identification)
) 
WITH comment = 'This tracks the YTD sales for each book we offer our customers.';

CREATE TABLE Royalty_History (
     Royalty_History_Identification text ,
     Order_Number int ,
     Item_Sequence_Number int ,
     Royalty_Payment_Date date ,
     Royalty_Payment_Amount  ,
     Royalty_Payee text ,
     PRIMARY KEY (Royalty_History_Identification)
) 
WITH comment = 'This tracks the history of royalty payments made by our company to an author for the books we sold.  ';

CREATE TABLE Reporting_Structure (
     Manager text ,
     Reports_To text ,
     Start_Date date ,
     End_Date date ,
     PRIMARY KEY ((Employee_Identification, Employee_Identification))
) 
WITH comment = 'This information further clarifies the kinds of approaches in which our company can accept payment for the purchase of books.';

CREATE TABLE Customer (
     Customer_Identification text ,
     Customer_First_Name text ,
     Customer_Last_Name text ,
     Customer_Company_Name text ,
     Customer_Street_Address text ,
     Customer_City text ,
     Customer_State ascii ,
     Customer_Zip_Code ascii ,
     Customer_Phone_Area_Code int ,
     Customer_Phone_Number int ,
     Customer_Fax_Area_Code int ,
     Customer_Fax_Number int ,
     PRIMARY KEY (Customer_Identification)
) 
WITH comment = ' A person or company that has either purchased a Quill PRODUCT or has called us for technical support.';

CREATE TABLE Book_Retrun (
     Book_Return_Identification text ,
     Order_Number int ,
     Item_Sequence_Number int ,
     Book_Return_Date date ,
     PRIMARY KEY (Book_Return_Identification)
) 
WITH comment = 'This information enables us to track which books are returned based on a line item for a purchase order.';

CREATE TABLE Credit_Card (
     Payment_Number int ,
     Card_Number int ,
     Card_Expiration_Date date ,
     Credit_Card_Type text ,
     Card_Vendor_Name text ,
     Credit_Card_Amount varint ,
     PRIMARY KEY (Payment_Number)
) 
WITH comment = 'One of three ways our customers can pay for the books they purchase.';

CREATE TABLE Money_Order (
     Payment_Number int ,
     Money_Order_Number int ,
     Money_Order_Amount varint ,
     PRIMARY KEY (Payment_Number)
) 
WITH comment = 'One of three ways our customers can pay for the books they purchase.';

CREATE TABLE Personal_Check (
     Payment_Number int ,
     Check_Number int ,
     Check_Account_Number int ,
     Check_Bank_Number int ,
     Check_Driver_License_Number text ,
     Check_Amount varint ,
     PRIMARY KEY (Payment_Number)
) 
WITH comment = 'One of three ways our customers can pay for the books they purchase.';

CREATE TABLE Payment (
     Payment_Number int ,
     Payment_Type text ,
     Payment_Date date ,
     Payment_Amount decimal ,
     PRIMARY KEY (Payment_Number)
) 
WITH comment = 'Payment for a purchase order.
';

CREATE TABLE Region (
     Region_Identification text ,
     Region_Area text ,
     Region_Description text ,
     PRIMARY KEY (Region_Identification)
) 
WITH comment = 'The section of the USA where our company organizes sales tracking.';

CREATE TABLE Order_Shipment (
     Order_Shipment_Identifier text ,
     Order_Number int ,
     Item_Sequence_Number int ,
     Billing_Address text ,
     Shipping_Address text ,
     Shipment_Status text ,
     Scheduled_Shipment_Date date ,
     PRIMARY KEY ((Item_Sequence_Number, Order_Number, Order_Shipment_Identifier))
) 
WITH comment = 'The actual date in which a specific line item on a purchase order is shipped.';

CREATE TABLE Back_Order (
     Order_Shipment_Identifier text ,
     Order_Number int ,
     Item_Sequence_Number int ,
     Rescheduled_Shipment_Date date ,
     PRIMARY KEY ((Item_Sequence_Number, Order_Number, Order_Shipment_Identifier))
) 
WITH comment = 'This provides a means to track order line items that need to be back ordered.';

CREATE TABLE Credit_Check (
     Credit_Check_Event text ,
     Payment_Number int ,
     Credit_Check_Date date ,
     Credit_Status text ,
     PRIMARY KEY (Credit_Check_Event)
) 
WITH comment = 'One of three ways our customers can pay for the books they purchase.';