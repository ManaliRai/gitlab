
CREATE TABLE brg_securities_country
(
	country_key int NULL ,
	country_of_issue_key int NULL ,
	domicile_country_key int NULL ,
	incorporation_country_key int NULL ,
	iso_country_key int NULL ,
	market_exchange_country_key int NULL ,
	msci_country_key int NULL ,
	primary_exchange_country_key int NULL ,
	primary_listing_country_key int NULL ,
	reporting_country_key int NULL ,
	risk_country_key int NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id varchar(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL ,
	security_key int NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE brg_securities_issuer
(
	security_key int NOT NULL ,
	issuer_key int NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id varchar(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_asset_class
(
	id int NOT NULL ,
	asset_class_code NVARCHAR (50) NOT NULL ,
	asset_class_name NVARCHAR (100) NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id VARCHAR(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_asset_class
(
	id int NOT NULL ,
	asset_class_code NVARCHAR (50) NOT NULL ,
	asset_class_name NVARCHAR (100) NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_broker
(
	id int NOT NULL ,
	broker_code NVARCHAR (20) NOT NULL ,
	broker_name NVARCHAR (100) NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id VARCHAR(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_broker
(
	id int NOT NULL ,
	broker_code NVARCHAR (20) NOT NULL ,
	broker_name NVARCHAR (100) NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_broker
(
	id int NOT NULL ,
	broker_code NVARCHAR (20) NOT NULL ,
	broker_name NVARCHAR (100) NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_country
(
	id int NOT NULL ,
	country_code NVARCHAR (50) NOT NULL ,
	country_name NVARCHAR (50) NULL ,
	country_iso2_code NVARCHAR (100) NULL ,
	country_iso3_code NVARCHAR (100) NULL ,
	country_currency_code VARCHAR(30) NULL ,
	ctl_record_id VARBINARY(8000) NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id varchar(50) NULL ,
	ctl_current_flag int NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_currency
(
	currency_code VARCHAR(30) NOT NULL ,
	currency_name VARCHAR(100) NOT NULL ,
	ctl_record_id varbinary(8000) NOT NULL ,
	ctl_check_sum varbinary(8000) NULL ,
	ctl_valid_from_utc datetime2(7) NOT NULL ,
	ctl_valid_to_utc datetime2(7) NULL ,
	ctl_job_id varchar(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_date
(
	id int NOT NULL ,
	date date NOT NULL ,
	day_of_week varchar(10) NOT NULL ,
	day_of_month int NOT NULL ,
	month varchar(10) NOT NULL ,
	year int NOT NULL ,
	day_of_week_abbr varchar(10) NOT NULL ,
	month_nr int NOT NULL ,
	month_abbr varchar(10) NOT NULL ,
	financial_year_start datetime NOT NULL ,
	financial_year_end datetime NOT NULL ,
	fy_abbr varchar(10) NOT NULL ,
	fy_quarter varchar(10) NOT NULL ,
	end_of_month datetime NOT NULL ,
	end_of_business_month datetime NOT NULL ,
	business_day bit NOT NULL ,
	hard_close bit NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_date
(
	id int NOT NULL ,
	date date NOT NULL ,
	day_of_week varchar(10) NOT NULL ,
	day_of_month int NOT NULL ,
	month varchar(10) NOT NULL ,
	year int NOT NULL ,
	day_of_week_abbr varchar(10) NOT NULL ,
	month_nr int NOT NULL ,
	month_abbr varchar(10) NOT NULL ,
	financial_year_start datetime NOT NULL ,
	financial_year_end datetime NOT NULL ,
	fy_abbr varchar(10) NOT NULL ,
	fy_quarter varchar(10) NOT NULL ,
	end_of_month datetime NOT NULL ,
	end_of_business_month datetime NOT NULL ,
	business_day bit NOT NULL ,
	hard_close bit NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_issuer
(
	id int NOT NULL ,
	issuer_legal_entity_code NVARCHAR (50) NOT NULL ,
	issuer_legal_entity_name NVARCHAR (300) NULL ,
	parent_issuer_legal_entity_code NVARCHAR (50) NULL ,
	parent_issuer_legal_entity_name NVARCHAR (300) NULL ,
	global_parent_issuer_legal_entity_code NVARCHAR (50) NULL ,
	global_parent_issuer_legal_entity_name NVARCHAR (300) NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id varchar(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_issuer
(
	id int NOT NULL ,
	issuer_legal_entity_code NVARCHAR (50) NOT NULL ,
	issuer_legal_entity_name NVARCHAR (300) NULL ,
	parent_issuer_legal_entity_code NVARCHAR (50) NULL ,
	parent_issuer_legal_entity_name NVARCHAR (300) NULL ,
	global_parent_issuer_legal_entity_code NVARCHAR (50) NULL ,
	global_parent_issuer_legal_entity_name NVARCHAR (300) NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_option
(
	investone_account_number varchar(50) NOT NULL ,
	taa_code varchar(100) NULL ,
	saa_code varchar(100) NULL ,
	baa_code varchar(100) NULL ,
	overlay_sector varchar(100) NULL ,
	mtps_code varchar(100) NULL ,
	portal_investment_code varchar(100) NULL ,
	portal_investment_option_name varchar(100) NULL ,
	option_sort int NOT NULL ,
	ctl_record_id varbinary(8000) NOT NULL ,
	ctl_check_sum varbinary(8000) NULL ,
	ctl_valid_from_utc datetime2 NOT NULL ,
	ctl_valid_to_utc datetime2 NOT NULL ,
	ctl_job_id varchar(50) NOT NULL ,
	type varchar(100) NULL ,
	sub_type varchar(100) NULL ,
	inception_date date NOT NULL ,
	close_date date NULL ,
	companion_option_code varchar(100) NULL ,
	portfolio_group varchar(100) NULL ,
	member_flows_option_name varchar(100) NULL ,
	pearl_portfolio_code varchar(100) NULL ,
	id int NOT NULL ,
	option_code varchar(100) NOT NULL ,
	option_name varchar(100) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_option
(
	type varchar(100) NULL ,
	sub_type varchar(100) NULL ,
	close_date date NULL ,
	id int NOT NULL ,
	option_code varchar(100) NOT NULL ,
	option_name varchar(100) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_path
(
	id int NOT NULL ,
	full_path nvarchar(255) NOT NULL ,
	levels int NOT NULL ,
	level_1_code nvarchar(50) NOT NULL ,
	level_1_name nvarchar(255) NULL ,
	level_1_manager_type nvarchar(50) NULL ,
	level_1_ownership_type NVARCHAR(50) NULL ,
	level_2_code NVARCHAR(100) NULL ,
	level_2_name NVARCHAR(255) NULL ,
	level_2_type NVARCHAR(255) NULL ,
	level_3_code NVARCHAR(100) NULL ,
	level_3_name NVARCHAR(255) NULL ,
	level_3_type NVARCHAR(255) NULL ,
	level_4_code NVARCHAR(100) NULL ,
	level_4_name NVARCHAR(255) NULL ,
	level_4_type NVARCHAR(255) NULL ,
	level_5_code NVARCHAR(100) NULL ,
	level_5_name NVARCHAR(255) NULL ,
	level_5_type NVARCHAR(255) NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id VARCHAR(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_path
(
	id int NOT NULL ,
	full_path nvarchar(255) NOT NULL ,
	levels int NOT NULL ,
	level_1_code nvarchar(50) NOT NULL ,
	level_1_name nvarchar(255) NULL ,
	level_1_manager_type nvarchar(50) NULL ,
	level_1_ownership_type NVARCHAR(50) NULL ,
	level_2_code NVARCHAR(100) NULL ,
	level_2_name NVARCHAR(255) NULL ,
	level_2_type NVARCHAR(255) NULL ,
	level_3_code NVARCHAR(100) NULL ,
	level_3_name NVARCHAR(255) NULL ,
	level_3_type NVARCHAR(255) NULL ,
	level_4_code NVARCHAR(100) NULL ,
	level_4_name NVARCHAR(255) NULL ,
	level_4_type NVARCHAR(255) NULL ,
	level_5_code NVARCHAR(100) NULL ,
	level_5_name NVARCHAR(255) NULL ,
	level_5_type NVARCHAR(255) NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_portfolio
(
	id int NOT NULL ,
	portfolio_asset_class NVARCHAR(50) NULL ,
	portfolio_asset_class_name NVARCHAR(100) NULL ,
	portfolio_code NVARCHAR(50) NOT NULL ,
	portfolio_name NVARCHAR (255) NULL ,
	portfolio_type NVARCHAR (50) NULL ,
	portfolio_management_type NVARCHAR (50) NULL ,
	portfolio_classification_code NVARCHAR (50) NULL ,
	portfolio_classification_name NVARCHAR (100) NULL ,
	is_underlying bit NULL ,
	portfolio_currency_code NVARCHAR (50) NULL ,
	portfolio_currency_name NVARCHAR (50) NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id VARCHAR(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_portfolio
(
	id int NOT NULL ,
	portfolio_asset_class NVARCHAR(50) NULL ,
	portfolio_asset_class_name NVARCHAR(100) NULL ,
	portfolio_code NVARCHAR(50) NOT NULL ,
	portfolio_name NVARCHAR (255) NULL ,
	portfolio_type NVARCHAR (50) NULL ,
	portfolio_management_type NVARCHAR (50) NULL ,
	portfolio_classification_code NVARCHAR (50) NULL ,
	portfolio_classification_name NVARCHAR (100) NULL ,
	is_underlying bit NULL ,
	portfolio_currency_code NVARCHAR (50) NULL ,
	portfolio_currency_name NVARCHAR (50) NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_sector
(
	id int NOT NULL ,
	sector_name varchar(100) NOT NULL ,
	category varchar(30) NOT NULL ,
	sector_code varchar(10) NOT NULL ,
	abor_sector_code varchar(10) NOT NULL ,
	pearl_sector_code varchar(30) NOT NULL ,
	overlay_option_code varchar(30) NOT NULL ,
	portfolio_name varchar(200) NOT NULL ,
	portfolio_code varchar(30) NOT NULL ,
	parent_portfolio_name_1 varchar(200) NOT NULL ,
	parent_portfolio_code_1 varchar(30) NOT NULL ,
	parent_portfolio_name_2 varchar(200) NOT NULL ,
	parent_portfolio_code_2 varchar(30) NOT NULL ,
	parent_portfolio_name_3 varchar(200) NOT NULL ,
	parent_portfolio_code_3 varchar(30) NOT NULL ,
	parent_portfolio_name_4 varchar(200) NOT NULL ,
	parent_portfolio_code_4 varchar(30) NOT NULL ,
	parent_portfolio_name_5 varchar(200) NOT NULL ,
	parent_portfolio_code_5 varchar(30) NOT NULL ,
	parent_portfolio_name_6 varchar(200) NOT NULL ,
	parent_portfolio_code_6 varchar(30) NOT NULL ,
	parent_portfolio_name_7 varchar(200) NOT NULL ,
	parent_portfolio_code_7 varchar(30) NOT NULL ,
	parent_portfolio_name_8 varchar(200) NOT NULL ,
	parent_portfolio_code_8 varchar(30) NOT NULL ,
	parent_portfolio_name_9 varchar(200) NOT NULL ,
	parent_portfolio_code_9 varchar(30) NOT NULL ,
	parent_portfolio_name_10 varchar(200) NOT NULL ,
	parent_portfolio_code_10 varchar(30) NOT NULL ,
	source_reference_id int NOT NULL ,
	source_insert_date date NULL ,
	source_delete_date date NULL ,
	ctl_record_id varbinary(8000) NOT NULL ,
	ctl_check_sum varbinary(8000) NOT NULL ,
	ctl_valid_from_utc datetime2 NOT NULL ,
	ctl_valid_to_utc datetime2 NOT NULL ,
	ctl_job_id varchar(50) NOT NULL ,
	ctl_current_flag integer NOT NULL ,
	ctl_record_insert_utc datetime2 NOT NULL ,
	ctl_record_update_utc datetime2 NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_sector
(
	id int NOT NULL ,
	sector_name varchar(100) NOT NULL ,
	category varchar(30) NOT NULL ,
	sector_code varchar(10) NOT NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_securities
(
	id int NOT NULL ,
	instrument_id NVARCHAR (265) NOT NULL ,
	instrument_name NVARCHAR (255) NULL ,
	instrument_long_name NVARCHAR (255) NULL ,
	instrument_type NVARCHAR (100) NULL ,
	instrument_sub_type NVARCHAR (100) NULL ,
	asset_category_class NVARCHAR (50) NULL ,
	asset_category_type NVARCHAR (50) NULL ,
	industry_sector_code NVARCHAR (50) NULL ,
	industry_sector_name NVARCHAR (255) NULL ,
	industry_group_code NVARCHAR (50) NULL ,
	industry_group_name NVARCHAR (255) NULL ,
	industry_code NVARCHAR (50) NULL ,
	industry_name NVARCHAR (255) NULL ,
	sub_industry_code NVARCHAR (50) NULL ,
	sub_industry_name NVARCHAR (255) NULL ,
	sector NVARCHAR (50) NULL ,
	sub_sector NVARCHAR (50) NULL ,
	ownership_type NVARCHAR (50) NULL ,
	strategy_type NVARCHAR (50) NULL ,
	strategy NVARCHAR (100) NULL ,
	cusip NVARCHAR (20) NULL ,
	isin NVARCHAR (20) NULL ,
	sedol NVARCHAR (20) NULL ,
	bloomberg_code NVARCHAR (50) NULL ,
	other_identifier NVARCHAR (50) NULL ,
	ric NVARCHAR (50) NULL ,
	contract_code NVARCHAR (50) NULL ,
	ticker NVARCHAR (50) NULL ,
	parent_instrument_id NVARCHAR (50) NULL ,
	loan_id NVARCHAR (50) NULL ,
	underlier_code NVARCHAR (50) NULL ,
	underlier_name NVARCHAR (100) NULL ,
	underlier_type NVARCHAR (255) NULL ,
	underlier_source NVARCHAR (100) NULL ,
	category_code NVARCHAR (50) NULL ,
	sub_category_code NVARCHAR (50) NULL ,
	market_exchange_code NVARCHAR (20) NULL ,
	market_exchange_name NVARCHAR (100) NULL ,
	structure NVARCHAR (50) NULL ,
	registration NVARCHAR (50) NULL ,
	multi_floater bit NULL ,
	life_floor DECIMAL (36,9) NULL ,
	lien NVARCHAR (50) NULL ,
	index_name NVARCHAR (50) NULL ,
	tranche NVARCHAR (50) NULL ,
	facility_level_spread DECIMAL (36,9) NULL ,
	amortisation_indicator bit NULL ,
	coupon_type NVARCHAR (50) NULL ,
	coupon_frequency_code int NULL ,
	coupon_frequency_name NVARCHAR (20) NULL ,
	latitude DECIMAL (36,9) NULL ,
	longitude DECIMAL (36,9) NULL ,
	holding_status NVARCHAR (10) NULL ,
	option_type NVARCHAR (20) NULL ,
	factor DECIMAL (36,9) NULL ,
	convertible bit NULL ,
	manager_name NVARCHAR (255) NULL ,
	sector_level NVARCHAR (50) NULL ,
	sector_level0 NVARCHAR (50) NULL ,
	sector_level1 NVARCHAR (50) NULL ,
	sector_level2 NVARCHAR (50) NULL ,
	sector_level3 NVARCHAR (50) NULL ,
	sector_level5 NVARCHAR (50) NULL ,
	geography_class NVARCHAR (50) NULL ,
	instrument_status NVARCHAR (20) NULL ,
	pe_level1 NVARCHAR (100) NULL ,
	pe_level2 NVARCHAR (100) NULL ,
	pe_level3 NVARCHAR (100) NULL ,
	pe_level4 NVARCHAR (100) NULL ,
	pe_level5 NVARCHAR (100) NULL ,
	private_equity_classification NVARCHAR (255) NULL ,
	b_class1_name NVARCHAR (255) NULL ,
	b_class1_index_code NVARCHAR (50) NULL ,
	b_class2_name NVARCHAR (255) NULL ,
	b_class2_index_code NVARCHAR (50) NULL ,
	b_class3_name NVARCHAR (255) NULL ,
	b_class3_index_code NVARCHAR (50) NULL ,
	b_class4_name NVARCHAR (255) NULL ,
	b_class4_index_code NVARCHAR (50) NULL ,
	fi_rating_classification_code NVARCHAR (50) NULL ,
	fi_rating_classification_name NVARCHAR (150) NULL ,
	share_class NVARCHAR (100) NULL ,
	investment_type NVARCHAR (50) NULL ,
	vintage_year NVARCHAR (20) NULL ,
	risk_currency_code VARCHAR(30) NULL ,
	thomson_reuters_classification_code NVARCHAR (50) NULL ,
	thomson_reuters_classification_name NVARCHAR (255) NULL ,
	total_global_amount DECIMAL (36,9) NULL ,
	original_commitment_size DECIMAL (36,9) NULL ,
	lin NVARCHAR (255) NULL ,
	gl_code NVARCHAR (50) NULL ,
	currency_code VARCHAR(30) NULL ,
	exposure_currency_code VARCHAR(30) NULL ,
	buy_currency_code VARCHAR(30) NULL ,
	sell_currency_code VARCHAR(30) NULL ,
	reporting_currency_code VARCHAR(30) NULL ,
	trading_currency_code VARCHAR(30) NULL ,
	denomination_currency_code VARCHAR(30) NULL ,
	sub_asset_class_code NVARCHAR (50) NULL ,
	sub_asset_class_name NVARCHAR (100) NULL ,
	cash_type_code NVARCHAR (50) NULL ,
	cash_type_name NVARCHAR (100) NULL ,
	risk_profile_code NVARCHAR (50) NULL ,
	risk_profile_name NVARCHAR (100) NULL ,
	date_convention_code NVARCHAR (50) NULL ,
	date_convention_name NVARCHAR (100) NULL ,
	contract_id NVARCHAR (50) NULL ,
	pay_or_rec_indicator NVARCHAR (20) NULL ,
	leg_type NVARCHAR (20) NULL ,
	leg_index NVARCHAR (50) NULL ,
	leg_interest_term NVARCHAR (20) NULL ,
	reset_frequency_code int NULL ,
	reset_frequency_name NVARCHAR (20) NULL ,
	contract_amortisation_indicator NVARCHAR (20) NULL ,
	instrument_contract_status NVARCHAR (20) NULL ,
	cdx_type NVARCHAR (20) NULL ,
	cdx_term NVARCHAR (20) NULL ,
	cdx_series NVARCHAR (20) NULL ,
	cdx_index NVARCHAR (20) NULL ,
	contract_payment_calendar NVARCHAR (50) NULL ,
	deal_commitment_local DECIMAL (36,9) NULL ,
	deal_unfunded_commitment_local DECIMAL (36,9) NULL ,
	description NVARCHAR (255) NULL ,
	product_type NVARCHAR (50) NULL ,
	payment_bus_day_convention_code NVARCHAR (50) NULL ,
	payment_bus_day_convention_name NVARCHAR (100) NULL ,
	swap_underlier_code NVARCHAR (50) NULL ,
	swap_underlier_name NVARCHAR (255) NULL ,
	swap_underlier_type NVARCHAR (50) NULL ,
	swap_underlier_isin NVARCHAR (50) NULL ,
	brscusip NVARCHAR (100) NULL ,
	ctl_record_id VARBINARY(8000) NOT NULL ,
	ctl_check_sum VARBINARY(8000) NULL ,
	ctl_valid_from_utc DATETIME2(7) NOT NULL ,
	ctl_valid_to_utc DATETIME2(7) NOT NULL ,
	ctl_job_id varchar(50) NOT NULL ,
	ctl_current_flag int NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	ctl_record_update_utc DATETIME2(7) NOT NULL ,
	trade_date int NULL ,
	effective_date int NULL ,
	maturity_date int NULL ,
	credit_agreement_execution_date int NULL ,
	first_coupon_payment_date int NULL ,
	issue_date int NULL ,
	accrual_date int NULL ,
	first_reset_date int NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE dim_securities
(
	id int NOT NULL ,
	instrument_id NVARCHAR (265) NOT NULL ,
	instrument_name NVARCHAR (255) NULL ,
	instrument_long_name NVARCHAR (255) NULL ,
	instrument_type NVARCHAR (100) NULL ,
	instrument_sub_type NVARCHAR (100) NULL ,
	asset_category_class NVARCHAR (50) NULL ,
	asset_category_type NVARCHAR (50) NULL ,
	industry_sector_code NVARCHAR (50) NULL ,
	industry_sector_name NVARCHAR (255) NULL ,
	industry_group_code NVARCHAR (50) NULL ,
	industry_group_name NVARCHAR (255) NULL ,
	industry_code NVARCHAR (50) NULL ,
	industry_name NVARCHAR (255) NULL ,
	sub_industry_code NVARCHAR (50) NULL ,
	sub_industry_name NVARCHAR (255) NULL ,
	sector NVARCHAR (50) NULL ,
	sub_sector NVARCHAR (50) NULL ,
	ownership_type NVARCHAR (50) NULL ,
	strategy_type NVARCHAR (50) NULL ,
	strategy NVARCHAR (100) NULL ,
	cusip NVARCHAR (20) NULL ,
	isin NVARCHAR (20) NULL ,
	sedol NVARCHAR (20) NULL ,
	bloomberg_code NVARCHAR (50) NULL ,
	other_identifier NVARCHAR (50) NULL ,
	ric NVARCHAR (50) NULL ,
	contract_code NVARCHAR (50) NULL ,
	ticker NVARCHAR (50) NULL ,
	parent_instrument_id NVARCHAR (50) NULL ,
	loan_id NVARCHAR (50) NULL ,
	underlier_code NVARCHAR (50) NULL ,
	underlier_name NVARCHAR (100) NULL ,
	underlier_type NVARCHAR (255) NULL ,
	underlier_source NVARCHAR (100) NULL ,
	category_code NVARCHAR (50) NULL ,
	sub_category_code NVARCHAR (50) NULL ,
	market_exchange_code NVARCHAR (20) NULL ,
	market_exchange_name NVARCHAR (100) NULL ,
	structure NVARCHAR (50) NULL ,
	registration NVARCHAR (50) NULL ,
	multi_floater bit NULL ,
	life_floor DECIMAL (36,9) NULL ,
	lien NVARCHAR (50) NULL ,
	index_name NVARCHAR (50) NULL ,
	tranche NVARCHAR (50) NULL ,
	facility_level_spread DECIMAL (36,9) NULL ,
	amortisation_indicator bit NULL ,
	coupon_type NVARCHAR (50) NULL ,
	coupon_frequency_code int NULL ,
	coupon_frequency_name NVARCHAR (20) NULL ,
	latitude DECIMAL (36,9) NULL ,
	longitude DECIMAL (36,9) NULL ,
	holding_status NVARCHAR (10) NULL ,
	option_type NVARCHAR (20) NULL ,
	factor DECIMAL (36,9) NULL ,
	convertible bit NULL ,
	manager_name NVARCHAR (255) NULL ,
	sector_level NVARCHAR (50) NULL ,
	sector_level0 NVARCHAR (50) NULL ,
	sector_level1 NVARCHAR (50) NULL ,
	sector_level2 NVARCHAR (50) NULL ,
	sector_level3 NVARCHAR (50) NULL ,
	sector_level5 NVARCHAR (50) NULL ,
	geography_class NVARCHAR (50) NULL ,
	instrument_status NVARCHAR (20) NULL ,
	pe_level1 NVARCHAR (100) NULL ,
	pe_level2 NVARCHAR (100) NULL ,
	pe_level3 NVARCHAR (100) NULL ,
	pe_level4 NVARCHAR (100) NULL ,
	pe_level5 NVARCHAR (100) NULL ,
	private_equity_classification NVARCHAR (255) NULL ,
	b_class1_name NVARCHAR (255) NULL ,
	b_class1_index_code NVARCHAR (50) NULL ,
	b_class2_name NVARCHAR (255) NULL ,
	b_class2_index_code NVARCHAR (50) NULL ,
	b_class3_name NVARCHAR (255) NULL ,
	b_class3_index_code NVARCHAR (50) NULL ,
	b_class4_name NVARCHAR (255) NULL ,
	b_class4_index_code NVARCHAR (50) NULL ,
	fi_rating_classification_code NVARCHAR (50) NULL ,
	fi_rating_classification_name NVARCHAR (150) NULL ,
	share_class NVARCHAR (100) NULL ,
	investment_type NVARCHAR (50) NULL ,
	vintage_year NVARCHAR (20) NULL ,
	risk_currency_code NVARCHAR(50) NULL ,
	thomson_reuters_classification_code NVARCHAR (50) NULL ,
	thomson_reuters_classification_name NVARCHAR (255) NULL ,
	total_global_amount DECIMAL (36,9) NULL ,
	original_commitment_size DECIMAL (36,9) NULL ,
	lin NVARCHAR (255) NULL ,
	gl_code NVARCHAR (50) NULL ,
	currency_code NVARCHAR(50) NULL ,
	exposure_currency_code NVARCHAR(50) NULL ,
	buy_currency_code NVARCHAR(50) NULL ,
	sell_currency_code NVARCHAR(50) NULL ,
	reporting_currency_code NVARCHAR(50) NULL ,
	trading_currency_code NVARCHAR(50) NULL ,
	denomination_currency_code NVARCHAR(50) NULL ,
	sub_asset_class_code NVARCHAR (50) NULL ,
	sub_asset_class_name NVARCHAR (100) NULL ,
	cash_type_code NVARCHAR (50) NULL ,
	cash_type_name NVARCHAR (100) NULL ,
	risk_profile_code NVARCHAR (50) NULL ,
	risk_profile_name NVARCHAR (100) NULL ,
	date_convention_code NVARCHAR (50) NULL ,
	date_convention_name NVARCHAR (100) NULL ,
	contract_id NVARCHAR (50) NULL ,
	pay_or_rec_indicator NVARCHAR (20) NULL ,
	leg_type NVARCHAR (20) NULL ,
	leg_index NVARCHAR (50) NULL ,
	leg_interest_term NVARCHAR (20) NULL ,
	reset_frequency_code int NULL ,
	reset_frequency_name NVARCHAR (20) NULL ,
	contract_amortisation_indicator NVARCHAR (20) NULL ,
	instrument_contract_status NVARCHAR (20) NULL ,
	cdx_type NVARCHAR (20) NULL ,
	cdx_term NVARCHAR (20) NULL ,
	cdx_series NVARCHAR (20) NULL ,
	cdx_index NVARCHAR (20) NULL ,
	contract_payment_calendar NVARCHAR (50) NULL ,
	deal_commitment_local DECIMAL (36,9) NULL ,
	deal_unfunded_commitment_local DECIMAL (36,9) NULL ,
	description NVARCHAR (255) NULL ,
	product_type NVARCHAR (50) NULL ,
	payment_bus_day_convention_code NVARCHAR (50) NULL ,
	payment_bus_day_convention_name NVARCHAR (100) NULL ,
	swap_underlier_code NVARCHAR (50) NULL ,
	swap_underlier_name NVARCHAR (255) NULL ,
	swap_underlier_type NVARCHAR (50) NULL ,
	swap_underlier_isin NVARCHAR (50) NULL ,
	brscusip NVARCHAR (100) NULL ,
	market_exchange_country_code NVARCHAR (50) NULL ,
	market_exchange_country_name NVARCHAR (50) NULL ,
	market_exchange_country_currency_code NVARCHAR (50) NULL ,
	market_exchange_country_currency_name NVARCHAR (50) NULL ,
	market_exchange_country_iso2_code NVARCHAR (100) NULL ,
	market_exchange_country_iso3_code NVARCHAR (100) NULL ,
	risk_currency_name NVARCHAR (50) NULL ,
	country_code NVARCHAR (50) NULL ,
	country_name NVARCHAR (50) NULL ,
	country_currency_code NVARCHAR (50) NULL ,
	country_currency_name NVARCHAR (50) NULL ,
	country_iso2_code NVARCHAR (100) NULL ,
	country_iso3_code NVARCHAR (100) NULL ,
	reporting_country_code NVARCHAR (50) NULL ,
	reporting_country_name NVARCHAR (50) NULL ,
	reporting_country_currency_code NVARCHAR (50) NULL ,
	reporting_country_currency_name NVARCHAR (50) NULL ,
	reporting_country_iso2_code NVARCHAR (100) NULL ,
	reporting_country_iso3_code NVARCHAR (100) NULL ,
	risk_country_code NVARCHAR (50) NULL ,
	risk_country_name NVARCHAR (50) NULL ,
	risk_country_currency_code NVARCHAR (50) NULL ,
	risk_country_currency_name NVARCHAR (50) NULL ,
	risk_country_iso2_code NVARCHAR (100) NULL ,
	risk_country_iso3_code NVARCHAR (100) NULL ,
	domicile_country_code NVARCHAR (50) NULL ,
	domicile_country_name NVARCHAR (50) NULL ,
	domicile_country_currency_code NVARCHAR (50) NULL ,
	domicile_country_currency_name NVARCHAR (50) NULL ,
	domicile_country_iso2_code NVARCHAR (100) NULL ,
	domicile_country_iso3_code NVARCHAR (100) NULL ,
	primary_listing_country_code NVARCHAR (50) NULL ,
	primary_listing_country_name NVARCHAR (50) NULL ,
	primary_listing_country_currency_code NVARCHAR (50) NULL ,
	primary_listing_country_currency_name NVARCHAR (50) NULL ,
	primary_listing_country_iso2_code NVARCHAR (100) NULL ,
	primary_listing_country_iso3_code NVARCHAR (100) NULL ,
	msci_country_code NVARCHAR (50) NULL ,
	msci_country_name NVARCHAR (50) NULL ,
	msci_country_currency_code NVARCHAR (50) NULL ,
	msci_country_currency_name NVARCHAR (50) NULL ,
	msci_country_iso2_code NVARCHAR (100) NULL ,
	msci_country_iso3_code NVARCHAR (100) NULL ,
	iso_country_code NVARCHAR (50) NULL ,
	iso_country_name NVARCHAR (50) NULL ,
	iso_country_currency_code NVARCHAR (50) NULL ,
	iso_country_currency_name NVARCHAR (50) NULL ,
	iso_country_iso2_code NVARCHAR (100) NULL ,
	iso_country_iso3_code NVARCHAR (100) NULL ,
	incorporation_country_code NVARCHAR (50) NULL ,
	incorporation_country_name NVARCHAR (50) NULL ,
	incorporation_country_currency_code NVARCHAR (50) NULL ,
	incorporation_country_currency_name NVARCHAR (50) NULL ,
	incorporation_country_iso2_code NVARCHAR (100) NULL ,
	incorporation_country_iso3_code NVARCHAR (100) NULL ,
	primary_exchange_country_code NVARCHAR (50) NULL ,
	primary_exchange_country_name NVARCHAR (50) NULL ,
	primary_exchange_country_currency_code NVARCHAR (50) NULL ,
	primary_exchange_country_currency_name NVARCHAR (50) NULL ,
	primary_exchange_country_iso2_code NVARCHAR (100) NULL ,
	primary_exchange_country_iso3_code NVARCHAR (100) NULL ,
	country_of_issue_code NVARCHAR (50) NULL ,
	country_of_issue_name NVARCHAR (50) NULL ,
	country_of_issue_currency_code NVARCHAR (50) NULL ,
	country_of_issue_currency_name NVARCHAR (50) NULL ,
	country_of_issue_iso2_code NVARCHAR (100) NULL ,
	country_of_issue_iso3_code NVARCHAR (100) NULL ,
	currency_name NVARCHAR (50) NULL ,
	exposure_currency_name NVARCHAR (50) NULL ,
	buy_currency_name NVARCHAR (50) NULL ,
	sell_currency_name NVARCHAR (50) NULL ,
	reporting_currency_name NVARCHAR (50) NULL ,
	trading_currency_name NVARCHAR (50) NULL ,
	denomination_currency_name NVARCHAR (50) NULL ,
	issuer_key int NULL ,
	trade_date int NULL ,
	effective_date int NULL ,
	maturity_date int NULL ,
	first_reset_date int NULL ,
	credit_agreement_execution_date int NULL ,
	first_coupon_payment_date int NULL ,
	issue_date int NULL ,
	accrual_date int NULL 
)
WITH ( 
DISTRIBUTION = REPLICATE);

CREATE TABLE hc_fact_holdings
(
	id int NOT NULL ,
	units decimal(36,9) NULL ,
	market_price_local decimal(36,9) NULL ,
	exchange_rate_local_to_base decimal(36,9) NULL ,
	gross_market_value_local decimal(36,9) NULL ,
	gross_market_value_base decimal(36,9) NULL ,
	accrued_interest_base decimal(36,9) NULL ,
	root_security_path_key int NOT NULL ,
	ctl_snapshot_date Datetime2(7) NOT NULL ,
	ctl_job_id VARCHAR(50) NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	date_key int NOT NULL ,
	option_key int NOT NULL ,
	portfolio_key int NOT NULL ,
	sector_key int NOT NULL ,
	asset_class_key int NOT NULL ,
	security_key int NOT NULL ,
	first_root_security_key int NOT NULL ,
	last_root_security_key int NOT NULL ,
	issuer_key int NULL ,
	broker_key int NULL 
)
WITH ( 
DISTRIBUTION = Hash(date_key));

CREATE TABLE hc_fact_holdings
(
	id int NOT NULL ,
	units decimal(36,9) NULL ,
	market_price_local decimal(36,9) NULL ,
	exchange_rate_local_to_base decimal(36,9) NULL ,
	gross_market_value_local decimal(36,9) NULL ,
	gross_market_value_base decimal(36,9) NULL ,
	accrued_interest_base decimal(36,9) NULL ,
	root_security_path_key int NOT NULL ,
	date_key int NOT NULL ,
	option_key int NOT NULL ,
	portfolio_key int NOT NULL ,
	sector_key int NULL ,
	asset_class_key int NULL ,
	issuer_key int NULL ,
	security_key int NOT NULL ,
	first_root_security_key int NULL ,
	last_root_security_key int NULL ,
	broker_key int NULL 
)
WITH ( 
DISTRIBUTION = Hash(date_key));

CREATE TABLE sc_fact_holdings
(
	id int NOT NULL ,
	units decimal(36,9) NULL ,
	market_price_local decimal(36,9) NULL ,
	exchange_rate_local_to_base decimal(36,9) NULL ,
	gross_market_value_local decimal(36,9) NULL ,
	gross_market_value_base decimal(36,9) NULL ,
	accrued_interest_base decimal(36,9) NULL ,
	ctl_snapshot_date Datetime2(7) NOT NULL ,
	ctl_job_id VARCHAR(50) NOT NULL ,
	ctl_record_insert_utc DATETIME2(7) NOT NULL ,
	root_security_path_key int NOT NULL ,
	option_key int NOT NULL ,
	date_key int NOT NULL ,
	portfolio_key int NOT NULL ,
	sector_key int NULL ,
	asset_class_key int NULL ,
	security_key int NOT NULL ,
	first_root_security_key int NULL ,
	last_root_security_key int NULL ,
	issuer_key int NULL ,
	broker_key int NULL 
)
WITH ( 
DISTRIBUTION = Hash(date_key), 
PARTITION (date_key RANGE Right FOR VALUES ( 20170101
,20180101
,20190101
,20200101
,20210101
,20220101
,20230101
,20240101
,20250101
,20260101
,20270101
,20280101
,20290101
,20300101
,20310101
,20320101
,20330101
,20340101
,20350101
,20360101
,20370101
,20380101
,20390101 ) ));

CREATE TABLE sc_fact_holdings
(
	id int NOT NULL ,
	units decimal(36,9) NULL ,
	market_price_local decimal(36,9) NULL ,
	exchange_rate_local_to_base decimal(36,9) NULL ,
	gross_market_value_local decimal(36,9) NULL ,
	gross_market_value_base decimal(36,9) NULL ,
	accrued_interest_base decimal(36,9) NULL ,
	issuer_key int NULL ,
	portfolio_key int NOT NULL ,
	root_security_path_key int NOT NULL ,
	asset_class_key int NULL ,
	option_key int NOT NULL ,
	sector_key int NULL ,
	date_key int NOT NULL ,
	security_key int NOT NULL ,
	last_root_security_key int NULL ,
	first_root_security_key int NULL ,
	broker_key int NULL 
)
WITH ( 
DISTRIBUTION = Hash(date_key), 
PARTITION (date_key RANGE Right FOR VALUES ( 20170101
,20180101
,20190101
,20200101
,20210101
,20220101
,20230101
,20240101
,20250101
,20260101
,20270101
,20280101
,20290101
,20300101
,20310101
,20320101
,20330101
,20340101
,20350101
,20360101
,20370101
,20380101
,20390101 ) ));

ALTER TABLE brg_securities_country
	ADD CONSTRAINT XPKbrg_securities_country PRIMARY KEY NONCLUSTERED (security_key ASC) NOT ENFORCED ;

ALTER TABLE brg_securities_issuer
	ADD CONSTRAINT XPKbrg_securities_issuer PRIMARY KEY NONCLUSTERED (security_key ASC) NOT ENFORCED ;

ALTER TABLE dim_asset_class
	ADD CONSTRAINT XPKdim_asset_class PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_asset_class
	ADD CONSTRAINT XPKdim_asset_class PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_broker
	ADD CONSTRAINT XPKdim_broker PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_broker
	ADD CONSTRAINT XPKdim_broker PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_broker
	ADD CONSTRAINT XPKdim_broker PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_country
	ADD CONSTRAINT XPKdim_country PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_currency
	ADD CONSTRAINT XPKdim_currency PRIMARY KEY NONCLUSTERED (currency_code ASC) NOT ENFORCED ;

ALTER TABLE dim_date
	ADD CONSTRAINT XPKdim_date PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_date
	ADD CONSTRAINT XPKdim_date PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_issuer
	ADD CONSTRAINT XPKdim_issuer PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_issuer
	ADD CONSTRAINT XPKdim_issuer PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_option
	ADD CONSTRAINT XPKdim_option PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_option
	ADD CONSTRAINT XPKdim_option PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_path
	ADD CONSTRAINT XPKdim_path PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_path
	ADD CONSTRAINT XPKdim_path PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_portfolio
	ADD CONSTRAINT XPKdim_portfolio PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_portfolio
	ADD CONSTRAINT XPKdim_portfolio PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_sector
	ADD CONSTRAINT XPKdim_sector PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_sector
	ADD CONSTRAINT XPKdim_sector PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_securities
	ADD CONSTRAINT XPKdim_securities PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE dim_securities
	ADD CONSTRAINT XPKdim_securities PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE hc_fact_holdings
	ADD CONSTRAINT XPKhc_fact_holdings PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE hc_fact_holdings
	ADD CONSTRAINT XPKhc_fact_holdings PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE sc_fact_holdings
	ADD CONSTRAINT XPKsc_fact_holdings PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;

ALTER TABLE sc_fact_holdings
	ADD CONSTRAINT XPKsc_fact_holdings PRIMARY KEY NONCLUSTERED (id ASC) NOT ENFORCED ;
